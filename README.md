# README #
## Inspiration
Have you ever met the situation that you searched for a building on Google, and it shew that the building is only a 3-minute walk, 
but you just couldn’t find it? Yes, Google Maps sometimes just doesn't know our campus at all. But we, BLUE JAYS, do. 
So we want to build an app that can present the information which is only known by Hopkins students. 
Our ultimate goal is that by using our app students can gain full access to JHU campus life
## What it does
This app's map system embeds all short cuts of our campus, including tunnels and passages between buildings. 
We will give customize route planning based on different weather situations. 
For example, the app will prioritize the route going through buildings and arcades when raining. 
Also, when planning path for people with disabilities, we always avoid stairs.