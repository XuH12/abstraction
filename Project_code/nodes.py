
class Node:
    def __init__(self, id, coords:tuple, adj_roads=[]):
        self.id = id
        self.coords = coords
        self.adj_roads = adj_roads

    def set_id(self, new_id):
        self.id = new_id

    def set_coords(self, new_coords):
        self.coords = new_coords
    
    def set_adj_roads(self, new_adj_roads):
        self.adj_roads = new_adj_roads
    
    def append_adj_road(self, new_adj_road):
        self.adj_roads = new_adj_road

    def concat_adj_road(self, new_adj_roads):
        self.adj_roads += new_adj_roads

    def get_id(self):
        return self.id

    def get_coords(self):
        return self.coords
    
    def get_adj_roads(self):
        return self.adj_roads
    
    def __lt__(self, other):
        if isinstance(other, Node):
            return self.id < other.id
        return self.id < other

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.id == other.id
        return self.id < other
    
    def __hash__(self):
        return hash((self.id, self.coords[0]))

    
class FacilitiesEntrance(Node):
    def __init__(self, id, coords, name, functions, adj_roads=[], max_occupants=None, cur_occupants=None, departments=[]):
        super().__init__(id, coords, adj_roads)
        self.functions = functions
        self.departments = departments
        self.max_occupants = max_occupants
        self.cur_occupants = max_occupants
        self.name = name
    
    def __str__(self):
        return self.name

    def get_functions(self):
        return self.functions

    def get_name(self):
        return self.name
    
    def get_max_occupants(self):
        return self.max_occupants
    
    def get_cur_occupants(self):
        return self.cur_occupants
    
    def get_departments(self):
        return self.departments
    
    def set_function(self, new_functions):
        self.functions = new_functions
    
    def append_function(self, new_function):
        self.functions.append(new_function)

    def set_name(self, new_name):
        self.name = new_name
    
    def set_max_occupants(self, new_max_occupants):
        self.max_occupants = new_max_occupants
    
    def set_cur_occupants(self, new_cur_occupants):
        self.cur_occupants = new_cur_occupants
    
    def set_departments(self, new_departments):
        self.departments = new_departments
    
class Junction(Node):
    def __init__(self, *arg, **kwargs):
        super().__init__(*arg, **kwargs)
        self.name = "NaN"
        self.functions = []
    
    def get_functions(self):
        return self.functions
    
    def get_name(self):
        return self.name
