
import Campus
#import stdio
import csv_loader
import math
import os
from queue import PriorityQueue
import find_path


def main():
    nodes_list = csv_loader.load_nodes_csv("data/nodes.csv")
    roads_list = csv_loader.load_roads_csv("data/road.csv")
    campus = Campus.Campus(roads_list,nodes_list)

    cur_nodes = getCurrentNodes(campus)
    weather_ind = getWeatherIndex()
    disability_ind = getDisabilityIndex()

    des_nodes = getDestination(campus)
    while (len(des_nodes) == 0):
        print("Please enter a valid location or function!\n")
        des_nodes = getDestination(campus)
    
    render_infos = [
        {0: [(0xda, 0x25, 0xb2), 6, "outdoor"], 1:[(0xDA, 0x25, 0x58), 6, "indoor"], "stair": [(0xA7, 0x25, 0xDA), 6, "stair"]},
        {0: [(0x51, 0x32, 0xCD), 12, "outdoor"], 1: [(0x9E, 0x32, 0xCD), 12, "indoor"], "stair": [(0x32, 0x61, 0xCD), 12, "stair"]}
    ]
    r_list, n_list = getPath(cur_nodes,des_nodes,weather_ind,disability_ind,campus)

    special_message(n_list, campus)
    paths_list = list(zip(r_list, n_list))
    paths_list.insert(0, (campus.roads, campus.nodes))
    image_path = os.path.join(os.getcwd(), "images")
    if(not os.path.exists(image_path)):
        os.makedirs(image_path)
    
    campus.render_all(paths_list, render_infos, fp=image_path, bg_file="jhu_background.png", out_file="result.png")
    
    """
    for r in r_list:
        campus.highlight_roads(r,(255, 70, 150),20, fp=image_path, bg_file="test.png", out_file="test_1.png")
    for n in n_list:
        campus.highlight_nodes(n,(255, 70, 150),(255, 70, 150),20, fp=image_path, bg_file="test.png", out_file="test_1.png")
    """

def special_message(nodes,campus):
    names=campus.get_all_node_names()
    for alist in nodes:
        for i in range(len(alist)):
            if (alist[i].name == "Fresh Food Café" and alist[i].name in names):
                print("Reminder for Fresh Food Café: Both Meal Swipes and Dining-dollar Accepted")
                names.remove(alist[i].name)
            elif (alist[i].name == "Bamboo Café" and alist[i].name in names):
                print("Reminder for Bamboo Café: Only Dining-dollar Accepted")
                names.remove(alist[i].name)
            elif (alist[i].name == "Brody Learning Common" and alist[i].name in names):
                print("Reminder for Brody Learning Common: Mainly Snack Food, only Dining-dollar")
                names.remove(alist[i].name)
            elif (alist[i].name == "Charles Common" and alist[i].name in names):
                print("Reminder for Charles Common: Both Meal Swipes and Dining-dollar Accepted, only Open on Evening")
                names.remove(alist[i].name)

"""
def findPath(start, end, weather, cripple, campus):
    explored=[]
    minDis={node:math.inf for node in campus.get_nodes_list()}
    minDis[start] = 0.0
    prev_nodes={}

    q = PriorityQueue()
    q.put((0.0, start))
    while not q.empty():
        pair = q.get()
        cur_node = pair[1]
        explored.append(cur_node) # the new one is updated
        if (cur_node == end):
            explored.append(cur_node)
            break
        adj_roads = [campus.get_road_by_id(id) for id in cur_node.get_adj_roads()]
        for road in adj_roads:
            new_node = campus.get_node_by_id(road.get_other_node(cur_node.get_id()))
            new_d = road.get_weighted_distance(weather, cripple) + minDis[cur_node]
            # if the new minDs is the minimum
            if new_d < minDis[new_node]:
                minDis[new_node] = new_d
                prev_nodes[new_node] = road
                if new_node not in explored:
                    q.put((new_d, new_node))

    # if q ended but 
    if(end not in explored):
        return (-1, [], [])
    # Reconstruct:
    road_list=[]
    node_list=[end]
    while  (node_list[0] != start):
        # print(node_list[0])
        # print(prev_nodes)
        prev_road = prev_nodes[node_list[0]]
        road_list.insert(0, prev_road)
        prev_node = campus.get_node_by_id(prev_road.get_other_node(node_list[0].get_id()))
        node_list.insert(0, prev_node)
    return (minDis[end], road_list, node_list) 
"""


def getWeatherIndex():
    weather = input("What is the weather today?(sunny, small rain, heavy rain)\n")
    while (weather != "sunny" and weather != "small rain" and weather != "heavy rain"):
        print("Please enter a valid weather!")
        weather = input("What is the weather today?(sunny, small rain, heavy rain)\n")
    if (weather == "sunny"):
        return 0
    elif (weather == "small rain"):
        return 0.5
    elif (weather == "heavy rain"):
        return 1

def getCurrentNodes(campus):
    s="What is your current location? Possible locations are:\n"
    names=campus.get_all_node_names()
    for name in names:
        if (name != "NaN"):
            s = s + name + "\n"
    cur_loc = input(s)
    cur_nodes = campus.get_nodes_by_name(cur_loc)
    while (len(cur_nodes) == 0):
        print("Please enter a valid location!\n")
        cur_loc = input(s)
        cur_nodes = campus.get_nodes_by_name(cur_loc)
    return cur_nodes

def getDisabilityIndex():
    disability = input("Do you have certain disability?(Yes/No)\n")
    while (disability != "Yes" and disability != "No"):
        print("Please enter a valid answer!\n")
        disability = input("Do you have certain disability?(Yes/No)\n")
    if (disability == "yes"):
        return 1
    else:
        return 0

def getDestination(campus):
    s="What is your destination, or what kind of place are you looking for?\nPossible destinations are:\n"
    names=campus.get_all_node_names()
    for name in names:
        s = s + name + "\n"
    s = s + "\n"
    s = s + "Possible kinds of places are:\n"
    functions = campus.get_all_node_functions()
    for function in functions:
            s = s + function + "\n"
    user_in = input(s)
    while (user_in not in functions and user_in not in names):
        print("Please enter a valid answer!\n")
        user_in = input(s)
    if (user_in in functions):
        return campus.get_nodes_by_function(user_in)
    else:
        return campus.get_nodes_by_name(user_in)


def getStart(cur_nodes,des_nodes):
    min_distance2 = 9999999
    min_node = cur_nodes[0]
    for i in range(len(cur_nodes)):
        distance2 = 0
        c_coords = cur_nodes[i].get_coords()
        for d in des_nodes:
            d_coords = d.get_coords()
            distance2 += (c_coords[0]-d_coords[0])**2 + (c_coords[1]-d_coords[1])**2
        avr_distance2 = distance2 / len(des_nodes)
        if (avr_distance2 < min_distance2):
            min_distance2 = avr_distance2
            min_node = cur_nodes[i]
    return min_node


def getPath(cur_nodes,des_nodes,weather_ind,disability_ind,campus):
    r_list = []
    n_list = []
    diction = {}
    cur_node = getStart(cur_nodes,des_nodes)
    for d in des_nodes:
        distance,r,n = find_path.findPath(cur_node,d,weather_ind,disability_ind,campus)
        if (d.get_name() in diction):
            if(distance < diction[d.get_name()][1]):
                diction[d.get_name()]= [d.get_id(),distance,r,n]
        else:
            diction[d.get_name()] = [d.get_id(),distance,r,n]
    for name in diction:
        r_list.append(diction[name][2])
        n_list.append(diction[name][3])
        print("Start: " + cur_node.get_name() + "  End: " + name + "  Distance is ", diction[name][1], end = "\n")
    return r_list, n_list



if __name__ == '__main__':
    main()
    
