import nodes
import csv
import Road

def load_nodes_csv(file_name):
    nodes_list = []
    with open(file_name, newline='') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            new_data = row
            new_data[0] = int(new_data[0])
            for i in (1, 4): # for coordiantes and adj_roads, they are converted to int list
                new_data[i] = [int(x) for x in new_data[i].split()]
            if (new_data[2]=="NaN"): # this is a new junction
                new_data = new_data[0:2] + new_data[4:5] # discards the irrelevant info
                nodes_list.append(nodes.Junction(*new_data)) # construct new_data
            else: # new buildings!!!
                new_data[3] = "" if new_data[3]=="NaN" else new_data[3] # converts NaN empty string
                new_data[3] = new_data[3].split() # converts functions to list
                nodes_list.append(nodes.FacilitiesEntrance(*new_data))
            # print(new_data)
    return nodes_list

def load_roads_csv(file_name):
    roads_list = []
    with open(file_name, "r", newline='') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            new_data = row
            new_data[0] = int(new_data[0]) # id
            for i in (2, 3, 5):
                new_data[i] = float(new_data[i])
            for i in (1, 6):
                new_data[i] = [int(x) for x in new_data[i].split()] # convert to list of int
            assert(len(new_data[1]) == 2)
            if(new_data[4]=="TRUE"):
                new_data[4] = True
            else:
                new_data[4] = False
            roads_list.append(Road.Road(*new_data))
            # print(new_data)
    return roads_list
import os

# nodes_lists = load_roads_csv("project_code/road_2.csv")
