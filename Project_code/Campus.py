#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 12 20:31:05 2020

@author: yuhengshi
"""
import os
from PIL import Image, ImageDraw, ImageFont
import math
import nodes
import Road
class Campus:
    #Calculates the LENGTH of road for every road, and stores it in their fields.
    def __init__(self,roads,nodes):
        self.roads=roads
        self.nodes=nodes
        for i in range(len(roads)):
            ids = roads[i].list_of_node_id
            nodes = [self.get_node_by_id(id) for id in ids]
            coords = [node.get_coords() for node in nodes]
            roads[i].length = ((coords[0][0]-coords[1][0])**2 + (coords[0][1]-coords[1][1])**2)**0.5
            """
            # 这里的情况是这样的...
            # 最早有一个bug，是如果在每个 i loop结尾重制co1/co2，
            # 那么后面就会报错(NoneType)，所以我就索性重新写了
            # 后来才发现，bug是因为之前 k = 0被放在 i loop 外面了...
            k = 0
            for j in range(len(nodes)):
                if(nodes[j].id==ids[0] or nodes[j].id==ids[1]):
                    if(k==0):
                        co1=nodes[j].coords
                        k=k+1
                    else:
                        co2=nodes[j].coords
            roads[i].length=math.sqrt((co1[0]-co2[0])**2+(co1[1]-co2[1])**2)
            co1=None
            co2=None
            """
  
    def get_nodes_for_road(self,road):
        nodes_of_road=[]
        ids = road.list_of_node_id
        for j in range(len(self.nodes)):
            if(self.nodes[j].id==ids[0] or self.nodes[j].id==ids[1]):
                nodes_of_road.append(self.nodes[j])
        return nodes_of_road
    
    #given one node of a road, returns the other node of the road.
    def get_new_node_for_road(self,road,node):
        ids = road.list_of_node_id
        if(node.id == ids[0]):
            id_search = ids[1]
        elif (node.id == ids[1]):
            id_search=ids[0]
        else: print("Error!!")
        
        for j in range(len(self.nodes)):
            if(self.nodes[j].id==id_search):
                new_node_of_road=self.nodes[j]
        return new_node_of_road


    def get_roads_for_node(self,node):
        roads_of_node=[]
        ids = node.adj_roads
        for j in range(len(self.roads)):
            if(self.roads[j].id in ids):
                roads_of_node.append(self.roads[j])
        return roads_of_node
    '''
    def get_roads_for_node(self, node):
        roads_of_node=[self.get_road_by_id(node.adj_roads[0]), self.get_road_by_id(node.adj_roads[1])]
        return roads_of_node
    '''
    
    def get_road_by_id(self, id):
        result = list(filter(lambda x: x.id==id, self.roads))
        if (len(result) == 0):
            return None
        return result[0]
    
    def get_node_by_id(self, id):
        result = list(filter(lambda x: x.get_id()==id, self.nodes))
        if (len(result) == 0):
            return None
        return result[0]
    
    def get_nodes_by_function(self, function):
        result = list(filter(lambda x: function in x.get_functions(), self.nodes))
        return result
    
    def get_nodes_by_name(self, name):
        result = list(filter(lambda x: name == x.get_name(), self.nodes))
        return result

    #image size:2732*2048
    def highlight_nodes(self, im, nodes, color_inside, color_outside, width, og_dim=(2249, 1710)):
        dim = im.size
        for i in range(len(nodes)):
            Coords=nodes[i].get_coords()
            tupleA=(
                Coords[0]*(dim[0]/og_dim[0]) - 15, 
                im.size[1]-Coords[1]*(dim[1]/og_dim[1]) - 15)
            tupleB=(
                Coords[0]*(dim[0]/og_dim[0]) + 15, 
                im.size[1]-Coords[1]*(dim[1]/og_dim[1]) + 15)
            draw = ImageDraw.Draw(im)
            draw.ellipse((tupleA,tupleB), fill=color_inside, width=width, outline=color_outside)
        
    def highlight_roads(self, im, roads, color, width, og_dim=(2249, 1710)):
        dim = im.size
        for i in range(len(roads)):
            Nodes=self.get_nodes_for_road(roads[i])
            # OK, our coordinate system starts from bottom left
            # but PIL(& other image editors) start from top left...
            tupleA=(
                Nodes[0].coords[0]*(dim[0]/og_dim[0]), 
                im.size[1]-Nodes[0].coords[1]*(dim[0]/og_dim[0]))
            tupleB=(
                Nodes[1].coords[0]*(dim[0]/og_dim[0]),
                im.size[1]-Nodes[1].coords[1]*(dim[0]/og_dim[0]))
            draw = ImageDraw.Draw(im)
            draw.line((tupleA,tupleB), fill=color, width=width)
    
    def highlight_roads_by_type(self, im, roads, render_infos, legend_coord, legends, og_dim=(2249, 1710)):
        # render_fin = {int road_type: (<tuple>color, <int>width, <string>name), ...}
        roads_by_type = {"stair": []}
        for road in roads:
            if road.render[0] not in roads_by_type:
                roads_by_type[road.render[0]] = [road]
            else:
                roads_by_type[road.render[0]].append(road)
            if road.stair > 0:
                roads_by_type["stair"].append(road)
        # The following is COPIED FROM ABOVE METHOD, 
        # bc there is no point opening file multi-time for editing...
        for road_type, roads in roads_by_type.items():
            # self.highlight_roads(roads, render_info[road_type][0], render_info[road_type][1], fp=fp, bg_file=bg_file, out_file=out_file)
            self.highlight_roads(im, roads, render_infos[road_type][0], render_infos[road_type][1])
            if render_infos[road_type] not in legends:
                legends.append(render_infos[road_type])
                legend_coord = self.render_line_legend(im, render_infos[road_type], legend_coord)
        # override for stair.
        self.highlight_roads(im, roads_by_type["stair"], render_infos["stair"][0], render_infos["stair"][1])
        return legend_coord
    
    """
    def highlight_nodes_by_type(self, im, nodes, render_infos, target_type, legend_coord, og_dim=(2249, 1710)):
        nodes_by_types = {}
        for node in nodes:
            functions = node.get_functions()
            if "NaN" in functions:
                continue
            for i in range(len(functions)):
                if (functions[i] not in nodes_by_types):
                    nodes_by_types[node.get] = [node]
                else:
                    nodes_by_types[functions[i]].append(node)
        for node_type, nodes in nodes_by_types.items():
            color = render_infos[node_type][0]
            width = render_infos[node_type][1]
            self.highlight_nodes(im, nodes, color, color, width)
        # Override the colour for the target type...
        color = render_infos[node_type][0]
    """

    def render_line_legend(self, im, render_info, legend_coord, size=100):
        # render_info = (<tuple>color, <int>width, <string>name)
        # Draw A reference line
        end_coord = (legend_coord[0] + size, legend_coord[1])
        draw = ImageDraw.Draw(im)
        draw.line((legend_coord,end_coord), fill=render_info[0], width=render_info[1])
        
        # Add description
        # font = ImageFont.load("arial.pil")
        # TODO change font size....
        font = ImageFont.truetype("Roboto-Medium.ttf", 80)
        draw.text((end_coord[0]+100, end_coord[1]-40), render_info[2], font=font, fill=(0, 0, 0))
        return (legend_coord[0], legend_coord[1] + 100) # shift legend downwards

    def render_end(self, im, nodes_list, render_info, og_dim=(2249, 1710)):
        end = nodes_list[-1:]
        dim = im.size
        self.highlight_nodes(im, end, render_info[0][0], render_info[1][0], 10, og_dim=(2249, 1710))
        end_coord = (end[0].coords[0]*(dim[0]/og_dim[0]), im.size[1]-end[0].coords[1]*(dim[0]/og_dim[0]))
        
        font = ImageFont.truetype("Roboto-Medium.ttf", 30)
        draw = ImageDraw.Draw(im)
        draw.text((end_coord[0]+30, end_coord[1]), end[0].get_name(), font=font, fill=(0, 0, 0))
        

    def render_all(self, paths_lists, render_infos, fp="/Users/yuhengshi/Desktop/Hophacks/", bg_file="MAP.PNG", out_file="test.png", og_dim=[2249, 1710]):
        # paths_lists = [road_list, node_list]
        # line_render_infos = [{type_name: (<tuple>color, <int>width, <string>name),...},{},...]
        with Image.open(os.path.join(fp, bg_file)) as im:
            legend_coord = (im.size[0] * 0.1, im.size[1] * 0.1)
            legends = []
            for idx, path_list in enumerate(paths_lists):
                render_idx = idx
                if len(render_infos) <= 2 and idx > 1:
                    render_idx = 1
                legend_coord = self.highlight_roads_by_type(im, path_list[0], render_infos[render_idx], legend_coord, legends)
                if(idx != 0):
                    self.render_end(im, path_list[1], render_infos[render_idx])
            # self.highlight_roads_by_type(im, paths_lists[0][0], render_infos[0])
            im.save(os.path.join(fp, out_file), "PNG")
    
    def get_nodes_list(self):
        return self.nodes

    def get_roads_list(self):
        return self.roads
    
    def get_all_node_names(self):
        names=[]
        for node in self.nodes:
            if (node.name not in names and node.name!="NaN"):
                names.append(node.name)
        return names

    def get_all_node_functions(self):
        functions=[]
        for node in self.nodes:
            for functi in node.functions:
                if ((functi not in functions) and functi != "NaN"):
                    functions.append(functi)
        return functions
        
        
        
        
        
        
        
        
        
        