from queue import PriorityQueue
import math

import csv_loader
import Campus
import nodes
import Road

def findPath(start, end, weather, cripple, campus):
    explored=[]
    minDis={node:math.inf for node in campus.get_nodes_list()}
    minDis[start] = 0.0
    prev_nodes={}
    print("Sart Node:", start.id, "End Node:", end.id)

    q = PriorityQueue()
    q.put((0.0, start))
    while not q.empty():
        pair = q.get()
        cur_node = pair[1]
        explored.append(cur_node) # the new one is updated
        if (cur_node == end):
            explored.append(cur_node)
            print("hi")
            break
        adj_roads = [campus.get_road_by_id(id) for id in cur_node.get_adj_roads()]
        print("Current node:", cur_node.id, end=" ")
        print(cur_node.get_adj_roads())
        # print(cur_node.get_adj_roads())
        for road in adj_roads:
            new_node = campus.get_node_by_id(road.get_other_node(cur_node.get_id()))
            new_d = road.get_weighted_distance(weather, cripple) + minDis[cur_node]
            # if the new minDs is the minimum
            if new_d < minDis[new_node]:
                print("new node:", new_node.id, "prev_rd:", road.id)
                minDis[new_node] = new_d
                prev_nodes[new_node] = road
                # print([(x.id, y.id) for x,y in prev_nodes.items()])
                if new_node not in explored:
                    q.put((new_d, new_node))

    # print(prev_nodes[campus.get_node_by_id(79)])
    # if q ended but without reaching destination
    if(end not in explored):
        return (-1, [], [])
    # Reconstruct:
    road_list=[]
    node_list=[end]
    while  (node_list[0] != start):
        print(node_list[0].id, end=" ")
        # print(prev_nodes)
        prev_road = prev_nodes[node_list[0]]
        road_list.insert(0, prev_road)
        prev_node = campus.get_node_by_id(prev_road.get_other_node(node_list[0].get_id()))
        node_list.insert(0, prev_node)
    return (minDis[end], road_list, node_list)
                

if __name__ == "__main__":
    # Start node:
    start_node = 92
    # End node:
    end_node = 1

    nodes_list = csv_loader.load_nodes_csv("project_code/nodes.csv")
    roads_list = csv_loader.load_roads_csv("project_code/road.csv")
    campus = Campus.Campus(roads_list, nodes_list)

    start = campus.get_node_by_id(start_node)
    end = campus.get_node_by_id(end_node)
    print(start.get_adj_roads(), end.get_adj_roads())
    distance, road_list, nodes_list = findPath(start, end, 1 , False, campus)
    print(distance, [x.id for x in road_list], [x.id for x in nodes_list])

    render_infos = [
        {0: [(0, 150, 0), 3, "outdoor"], 1:[(0, 255, 0), 3, "indoor"], "stair": [(0, 50, 0), 3, "stair"]},
        {0: [(255, 70, 150), 10, "outdoor"], 1: [(200, 70, 150), 10, "indoor"], "stair": [(255, 120, 150), 10, "stair"]}
        ]
    import os
    image_path = os.path.join(os.getcwd(), "images")
    if(not os.path.exists(image_path)):
        os.makedirs(image_path)
    # campus.highlight_roads(roads_list, (200, 70, 200), 5, fp=image_path, bg_file="jhu_background.png", out_file="test.png")
    campus.render_all([(campus.roads, campus.nodes), (road_list, nodes_list)], render_infos, fp=image_path, bg_file="jhu_background.png", out_file="test_new.png")
    # campus.highlight_nodes([start, end], (255, 70, 150), (100, 20, 200), 30, fp=image_path, bg_file="test_1.png", out_file="test_2.png")
    
